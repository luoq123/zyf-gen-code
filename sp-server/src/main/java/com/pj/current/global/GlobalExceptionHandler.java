package com.pj.current.global;

import java.sql.SQLException;

import cn.hutool.core.util.IdUtil;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.pj.utils.sg.AjaxError;
import com.pj.utils.sg.AjaxJson;


/**
 * 全局异常处理 
 * 
 * <p> @ControllerAdvice 可指定包前缀，例如：(basePackages = "com.pj.controller.admin")
 * @author kong
 *
 */
@RestControllerAdvice
public class GlobalExceptionHandler {

	/** 全局异常拦截  */
	@ExceptionHandler
	public AjaxJson handlerException(Exception e) {

		// 打印堆栈，以供调试
		e.printStackTrace(); 

    	// 记录日志信息
    	AjaxJson aj = null;
		Throwable e2 = e.getCause();
		
		// ------------- 判断异常类型，提供个性化提示信息 
		
		// 如果是AjaxError，则获取其具体code码
		if(e instanceof AjaxError) {
			AjaxError ee = (AjaxError) e;
			aj = AjaxJson.get(ee.getCode(), ee.getMessage());
		}  
		// 如果是SQLException，并且指定了hideSql，则只返回sql error 
		else if((e instanceof SQLException || e2 instanceof SQLException)) {
			// 无论是否打开隐藏sql，日志表记录的都是真实异常信息 
			aj = AjaxJson.getError(e2.getMessage());
			return AjaxJson.getError("Sql Error").set("reqId", IdUtil.fastSimpleUUID());
		}
		// 普通异常输出：500 + 异常信息 
		else {
			aj = AjaxJson.getError(e.getMessage());
		}
		
		// 返回到前台
		aj.set("reqId", IdUtil.fastSimpleUUID());
		return aj;
	}
	
}
