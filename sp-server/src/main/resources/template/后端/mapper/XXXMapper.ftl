package ${t.packageName}.${t.moduleName};

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.pj.utils.so.*;
import org.springframework.stereotype.Repository;

/**
* Controller: ${t.tableName} -- ${t.tableComment}
* @author ${t.authorName}
*/
@Mapper
@Repository
public interface ${t.entityName}Mapper {


}
