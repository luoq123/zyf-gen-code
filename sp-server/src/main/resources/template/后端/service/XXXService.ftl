package ${t.packageName}.${t.moduleName};

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
* Controller: ${t.tableName} -- ${t.tableComment}
* @author ${t.authorName}
*/
@Service
public class ${t.entityName}Service {

}
