package ${t.packageName}.${t.moduleName};

import java.io.Serializable;

import lombok.Data;
import lombok.experimental.Accessors;

/**
* Controller: ${t.tableName} -- ${t.tableComment}
* @author ${t.authorName}
*/
@Data
@Accessors(chain = true)
public class ${t.entityName} implements Serializable {

// ---------- 表中字段 ----------
<#list cs as c>
    /**
     * ${c.columnComment}
     */
    private ${c.javaType} ${c.javaField};
</#list>

}
